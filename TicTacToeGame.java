import java.util.Scanner;
public class TicTacToeGame
{
	public static void main(String[] args)
	{
		System.out.println("The TicTacToe game will now start!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
		System.out.println("Choose your row and column from a range of 1 to 3");
		
		Scanner scan = new Scanner(System.in);
		Board gameBoard = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
				
		while (gameOver != true)
		{
			System.out.println(gameBoard);
			System.out.println("Player " + player + ": it's your turn. Where do you want to place your token?");

			if(player == 1)
			{
				playerToken = Square.X;
			}
			else if(player == 2)
			{
				playerToken = Square.O;
			}
			
			int rowNum = scan.nextInt();
			int colNum = scan.nextInt();
			boolean placeResult = gameBoard.placeToken(rowNum, colNum, playerToken);
			while(placeResult == false)
			{
				System.out.println("Invalid input! Please pick again");
				rowNum = scan.nextInt();
				colNum = scan.nextInt();
				placeResult = gameBoard.placeToken(rowNum, colNum, playerToken);
			}
				
			boolean checkWin = gameBoard.checkIfWinning(playerToken);
			if(checkWin == true)
			{
				System.out.println(gameBoard);
				System.out.println("Player " + player + " is the winner!");
				gameOver = true;
			}
			else
			{
				boolean checkFull = gameBoard.checkIfFull();
				if(checkFull == true)
				{
					System.out.println(gameBoard);
					System.out.println("It's a tie!");
					gameOver = true;
				}
				else
				{
					if(player >= 2)
					{
						player = 1;
					}
					else
					{
						player++;
					}
				}
			}
		}
	}
}