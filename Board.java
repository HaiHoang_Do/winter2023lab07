public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		this.tictactoeBoard = new Square[][] { {Square.BLANK, Square.BLANK, Square.BLANK}, {Square.BLANK, Square.BLANK, Square.BLANK}, {Square.BLANK, Square.BLANK, Square.BLANK} };
	}
	
	public String toString()
	{
		String returnString = "";
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int I = 0; I < this.tictactoeBoard[i].length; I++)
			{
				returnString += this.tictactoeBoard[i][I] + " ";
			}
			returnString += "\r\n";
		}
		return returnString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		boolean posAvailable;
		if (row > 0 && col > 0 && row-1 < this.tictactoeBoard[0].length && col-1 < this.tictactoeBoard.length)
		{
			if(this.tictactoeBoard[row-1][col-1] == Square.BLANK)
			{
				this.tictactoeBoard[row-1][col-1] = playerToken;
				posAvailable = true;
			}
			else
			{
				posAvailable = false;
			}
		}
		else
		{
			posAvailable = false;
		}
		return posAvailable;
	}
	
	public boolean checkIfFull()
	{
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int I = 0; I < this.tictactoeBoard[i].length; I++)
			{
				if(this.tictactoeBoard[i][I] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		int matchesInARow;
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			matchesInARow = 0;
			for(int I = 0; I < this.tictactoeBoard[i].length; I++)
			{
				if(this.tictactoeBoard[i][I] == playerToken)
				{
					matchesInARow += 1;
				}
			}
			if(matchesInARow == 3)
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		int matchesInARow;
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			matchesInARow = 0;
			for(int I = 0; I < this.tictactoeBoard[i].length; I++)
			{
				if(this.tictactoeBoard[I][i] == playerToken)
				{
					matchesInARow += 1;
				}
			}
			if(matchesInARow == 3)
			{
				return true;
			}
		}
		return false;
	}
	
	
	public boolean checkIfWinning(Square playerToken)
	{
		boolean playerWinHor = checkIfWinningHorizontal(playerToken);
		boolean playerWinVer = checkIfWinningVertical(playerToken);
		if((playerWinHor == true) || (playerWinVer == true))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}